﻿using System;
using UnityEngine;

namespace _Core
{
    public class Alien : MonoBehaviour, IPoolableObject, IDestroyable
    {
        private const float bulletPositionX = 0.33F;
        private const float bulletPositionY = -0.22F;
        [SerializeField] private AlienParameters _parameters;
        [SerializeField] private BulletParameters _bulletParameters;
        private bool _isLeftGunShotLast;
        private int _currentHealth;
        private float _timeFromLastShot;
        private float _distanceLeft;
        private float _distance;
        private float _currentSpeed;
        private Vector2 _forwardDirection;
        private Vector2 _destination;
        private GameObject _target;
        private bool _isInitialized;

        public void Initialize(GameObject target)
        {
            _destination = transform.position;
            _target = target;
            _currentHealth = _parameters.MaxHp;
        }
        
        private void Update()
        {
            if (_isInitialized)
            {
                Move();
                AimOnTarget();
                Shoot();    
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (Utils.CheckCollision(other.gameObject, _parameters.DamageOnCollisionMask))
            {
                TakeDamage();
            }
            
        }
        private void TakeDamage()
        {
            _currentHealth -= 1;
            if (_currentHealth == 0)
            {
                DestroySelf();
            }
        }

        private void Move()
        {
            if (transform.position == (Vector3)_destination)
            {
                _destination = GameManager.GetRandomPositionInside();
                _distance = Vector2.Distance(transform.position, _destination);
                _currentSpeed =_parameters.MinSpeed;
            }
            _distanceLeft = Vector2.Distance(transform.position, _destination);
            if (_distanceLeft >= _distance * 0.5F)
            {
                _currentSpeed += _parameters.AccelerationSpeed * Time.deltaTime;
                if (_currentSpeed >= _parameters.MaxSpeed)
                {
                    _currentSpeed = _parameters.MaxSpeed;
                }
            }
            else
            {
                _currentSpeed -= _parameters.SlowdownSpeed * Time.deltaTime;
                if (_currentSpeed <= _parameters.MinSpeed)
                {
                    _currentSpeed = _parameters.MinSpeed;
                }
            }
            transform.position =
                Vector2.MoveTowards(transform.position, _destination, _parameters.MaxSpeed * Time.deltaTime);
            _forwardDirection = transform.rotation *  Vector2.down;
            
        }
        private void AimOnTarget()
        {
            float offset = 90F;
            Vector2 direction = _target.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0,0, angle + offset);
        }
        private void Shoot()
        {
            _timeFromLastShot += Time.deltaTime;
            if (_timeFromLastShot >= _parameters.ShootCooldown)
            {
                int gunPriority= DecideGunPriority();
                CreateAndShootBullet(gunPriority);
                _timeFromLastShot = 0;
            }
        }
        private int DecideGunPriority()
        {
            if (_isLeftGunShotLast)
            {
                _isLeftGunShotLast = false;
                return -1;
            }
            _isLeftGunShotLast = true;
            return 1;
        }
        private void CreateAndShootBullet(int gunPriority)
        {
            Vector2 bulletPosition = transform.TransformPoint
                ( new Vector3(bulletPositionX * gunPriority, bulletPositionY, 0));
            BulletBehaviour bullet = SpawnManager.SpawnBullet(bulletPosition);
            bullet.Initialize(_forwardDirection, _bulletParameters);
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }
        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public void DestroySelf()
        {
            SpawnManager.DestroyObject(this);
        }
        
        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }
    }
}