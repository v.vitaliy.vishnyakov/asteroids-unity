﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "AlienParameters", menuName = "Parameters/Alien Parameters", order = 2)]
    public class AlienParameters : ScriptableObject
    {
        [SerializeField] private int _maxHp = 3;
        [SerializeField] private int _score = 15;
        [SerializeField] private float maxSpeed = 3;
        [SerializeField] private float rotateSpeed = 1;
        [SerializeField] private float shootCooldown = 0.3F;
        [SerializeField] private float _accelerationSpeed = 1.5F;
        [SerializeField] private float _slowdownSpeed = 1.5F;
        [SerializeField] private float _minSpeed = 2F;
        [SerializeField] private LayerMask damageOnCollisionMask;

        public int MaxHp => _maxHp;
        public int Score => _score;
        public float MaxSpeed => maxSpeed;
        public float RotateSpeed => rotateSpeed;
        public float ShootCooldown => shootCooldown;
        public float AccelerationSpeed => _accelerationSpeed;
        public float SlowdownSpeed => _slowdownSpeed;
        public float MinSpeed => _minSpeed;
        public LayerMask DamageOnCollisionMask => damageOnCollisionMask;
    }
}