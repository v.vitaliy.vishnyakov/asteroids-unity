﻿using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random; 

namespace _Core
{
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public abstract class Asteroid : MonoBehaviour, IDestroyable, IPoolableObject, ITeleportable
    {
        [SerializeField] protected AsteroidParameters _parameters;
        private bool _isInitialized = false;
        private int _currentHp;
        private int _rotationDirection;
        private float _speed;
        private float _rotationSpeed;
        private Vector2 _direction;
        
        public void Initialize(Vector2 direction, float speed)
        {
            _direction = direction;
            _speed = speed;
            _isInitialized = true;
            _currentHp = _parameters.Hp;
            _rotationDirection = Random.Range(-2, 1) < 0 ? (-1) : 1;
            _rotationSpeed = Random.Range(0.5F, 1.5F);
        }
        
        private void Update()
        {
            if (_isInitialized)
                Move();
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (Utils.CheckCollision(other.gameObject, _parameters.DamageOnCollisionMask))
            {
                TakeDamage();
            }
        }
        
        private void OnDisable()
        {
            _isInitialized = false;
        }

        protected void Move()
        {
            transform.position += (Vector3)_direction * _speed * Time.deltaTime;
            transform.rotation *= quaternion.Euler(0,0,_rotationDirection*_rotationSpeed*Time.deltaTime);
        }
        
        protected void TakeDamage()
        {
            _currentHp -= 1;
            if (_currentHp == 0)
            {
                Break();   
            }
        }

        public void DestroySelf()
        {   
            SpawnManager.DestroyObject(this);
        }
        
        public abstract void Break();
        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        protected float CalculateSpeedForChild()
        {
            return Random.Range(0, _speed);
        }

        public void Teleport(Vector3 teleportPoint)
        {
            transform.position = teleportPoint;
        }

        public GameObject GetObject()
        {
            return gameObject;
        }
    }
}