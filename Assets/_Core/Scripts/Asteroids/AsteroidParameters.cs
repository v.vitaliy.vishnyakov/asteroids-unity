﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "AsteroidParameters", menuName = "Parameters/Asteroid Parameters", order = 1)]
    public class AsteroidParameters : ScriptableObject
    {
        [SerializeField] private int _hp;
        [SerializeField] private int _score;
        [SerializeField] private float _firstChildSpawnOffset;
        [SerializeField] private float _secondChildSpawnOffset;
        [SerializeField] private LayerMask _damageOnCollisionMask;
        [SerializeField] private string _breakSoundKey;
        public int Hp => _hp;
        public int Score => _score;
        public float FirstChildSpawnOffset => _firstChildSpawnOffset;
        public float SecondChildSpawnOffset => _secondChildSpawnOffset;
        public LayerMask DamageOnCollisionMask => _damageOnCollisionMask;
        public string BreakSoundKey => _breakSoundKey;
    }
}