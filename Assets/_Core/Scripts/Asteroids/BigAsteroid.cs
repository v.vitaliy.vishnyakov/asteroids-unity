﻿using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;

namespace _Core
{
    public class BigAsteroid : Asteroid
    {
        public override void Break()
        {
            Vector2 firstChildPos = transform.position * _parameters.FirstChildSpawnOffset;
            Vector2 secondChildPos = transform.position * _parameters.SecondChildSpawnOffset;
            SpawnManager.SpawnAsteroid<MidAsteroid>
                (firstChildPos, Random.insideUnitCircle.normalized, CalculateSpeedForChild());
            SpawnManager.SpawnAsteroid<MidAsteroid>
                (secondChildPos, Random.insideUnitCircle.normalized, CalculateSpeedForChild());
            GameManager.AddScore(_parameters.Score);
            SoundManager.PlaySFX(_parameters.BreakSoundKey);
            DestroySelf();
        }
    }
}