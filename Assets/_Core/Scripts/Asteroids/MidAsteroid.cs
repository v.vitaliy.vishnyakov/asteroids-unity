﻿using UnityEngine;

namespace _Core
{
    public class MidAsteroid : Asteroid
    {
        public override void Break()
        {
            Vector2 firstChildPos = transform.position * _parameters.FirstChildSpawnOffset;
            Vector2 secondChildPos = transform.position * _parameters.SecondChildSpawnOffset;
            SpawnManager.SpawnAsteroid<TinyAsteroid>
                (firstChildPos, Random.insideUnitCircle.normalized, CalculateSpeedForChild());
            SpawnManager.SpawnAsteroid<TinyAsteroid>
                (secondChildPos, Random.insideUnitCircle.normalized, CalculateSpeedForChild());
            GameManager.AddScore(_parameters.Score);
            SoundManager.PlaySFX(_parameters.BreakSoundKey);
            DestroySelf();
        }
    }
}