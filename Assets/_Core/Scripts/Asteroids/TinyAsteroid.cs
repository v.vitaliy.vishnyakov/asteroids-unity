﻿using UnityEngine;

namespace _Core
{
    public class TinyAsteroid : Asteroid
    {
        public override void Break()
        {
            GameManager.AddScore(_parameters.Score);
            SoundManager.PlaySFX(_parameters.BreakSoundKey);
            DestroySelf();
        }
    }
}