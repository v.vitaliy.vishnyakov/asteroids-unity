﻿using System.Collections;
using UnityEngine;

namespace _Core
{
    public abstract class Boost : MonoBehaviour, IPoolableObject
    {
        [SerializeField] protected BoostParameters _parameters;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                ApplyEffect();
                SoundManager.PlaySFX(_parameters.PickupSound);
                DestroySelf();    
            }
        }

        private void OnEnable()
        {
            StartCoroutine(WaitForDestruction());
        }

        protected abstract void ApplyEffect();

        private void DestroySelf()
        {
            SpawnManager.DestroyObject(this);
        }
        public void Enable()
        {
            gameObject.SetActive(true);
        }
        public void Disable()
        {
            gameObject.SetActive(false);
        }
        
        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        protected IEnumerator WaitForDestruction()
        {
            yield return new WaitForSeconds(_parameters.LifeTime);
            DestroySelf();
        }
    }
}