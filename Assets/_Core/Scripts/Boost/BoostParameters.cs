﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "BoostsParameters", menuName = "Parameters/Boosts Parameters", order = 2)]
    public class BoostParameters : ScriptableObject
    {
        [Header("Common")]
        [SerializeField] private float _lifeTime;
        [SerializeField] private float _boostSpawnMinTime;
        [SerializeField] private float _boostSpawnMaxTime;
        [SerializeField] private string _pickupSound;
        [Header("HpBoost")]
        [SerializeField] [Range(0.0f, 1.0f)] private float _hpBoostDropChance;
        [Header("ScoreBoost")]
        [SerializeField] [Range(0.0f, 1.0f)] private float _scoreBoostDropChance;
        [SerializeField] private int _scoreMultiplierMinValue;
        [SerializeField] private int _scoreMultiplierMaxValue;
        [Header("Credits")]
        [SerializeField] [Range(0.0f, 1.0f)] private float _creditsBoostDropChance;
        [SerializeField] private int _creditsAmount;

        public float LifeTime => _lifeTime;

        public float BoostSpawnMinTime => _boostSpawnMinTime;

        public float BoostSpawnMaxTime => _boostSpawnMaxTime;
        public string PickupSound => _pickupSound;
        public float HpBoostDropChance => _hpBoostDropChance;
        public float ScoreBoostDropChance => _scoreBoostDropChance;
        public int ScoreMultiplierMinValue => _scoreMultiplierMinValue;
        public int ScoreMultiplierMaxValue => _scoreMultiplierMaxValue;
        public float CreditsBoostDropChance => _creditsBoostDropChance;
        public int CreditsAmount => _creditsAmount;
    }
}