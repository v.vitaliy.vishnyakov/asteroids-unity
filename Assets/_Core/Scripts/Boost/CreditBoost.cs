﻿using UnityEngine;

namespace _Core
{
    public class CreditBoost : Boost
    {
        protected override void ApplyEffect()
        {
            GameManager.AddCredits(_parameters.CreditsAmount);
        }
    }
}