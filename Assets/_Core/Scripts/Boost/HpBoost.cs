﻿namespace _Core
{
    public class HpBoost : Boost
    {
        protected override void ApplyEffect()
        {
            GameManager.AddHp();
        }
    }
}