﻿using UnityEngine;

namespace _Core
{
    public class ScoreBoost : Boost
    {
        protected override void ApplyEffect()
        {
            int scoreModifier = Random.Range
                (_parameters.ScoreMultiplierMinValue, _parameters.ScoreMultiplierMaxValue);
            GameManager.SetScoreMultiplier(scoreModifier);
        }
    }
}