﻿using UnityEngine;

namespace _Core
{
    public class BulletBehaviour : MonoBehaviour, IDestroyable, IPoolableObject
    {
        private bool _isInitialized;
        private float _actualSpeed;
        private Vector3 _direction = Vector3.zero;
        private BulletParameters _parameters;

        public void Initialize(Vector3 direction, BulletParameters parameters, float initialSpeed = 0)
        {
            _parameters = parameters;
            gameObject.layer = _parameters.Layer;
            GetComponent<SpriteRenderer>().sprite = _parameters.BulletLook;
            _actualSpeed = initialSpeed + _parameters.Speed;
            _direction = direction;
            _isInitialized = true;
            SoundManager.PlaySFX(_parameters.BulletSoundKey);
        }
        
        private void Update()
        {
            if (_isInitialized)
            {
                Move();
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (Utils.CheckCollision(other.gameObject, _parameters.DestroyOnCollisionMask))
            {
                DestroySelf();
            }
        }

        private void Move()
        {
            transform.position += _direction * _actualSpeed * Time.deltaTime;
        }


        public void DestroySelf()
        {
            SpawnManager.DestroyObject(this);
        }
        
        public void Enable()
        {
            gameObject.SetActive(true);
        }
        public void Disable()
        {
            gameObject.SetActive(false);
        }
        private void OnDisable()
        {
            _isInitialized = false;
        }
        
        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }
    }
}