﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "BulletParameters", menuName = "Parameters/Bullet Parameters", order = 3)]
    public class BulletParameters : ScriptableObject
    {
        [SerializeField] private float _speed;
        [SerializeField] private Sprite _bulletLook;
        [SerializeField] private int _layer;
        [SerializeField] private LayerMask _destroyOnCollisionMask;
        [SerializeField] private string _bulletSoundKey;
        
        public float Speed => _speed;
        public Sprite BulletLook => _bulletLook;
        public int Layer => _layer;
        public LayerMask DestroyOnCollisionMask => _destroyOnCollisionMask;
        public string BulletSoundKey => _bulletSoundKey;
    }
}