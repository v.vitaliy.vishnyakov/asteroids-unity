﻿namespace _Core
{
    static class Tags
    {
        public const string Asteroid = "Asteroid";
        public const string Alien = "Alien";
        public const string Bullet = "Bullet";
        public const string Edge = "Edge";
        public const string Player = "Player";
        public const string GameArea = "GameArea";
    }
}