﻿using UnityEngine;

namespace _Core
{
    public class BackgroundBehaviour : MonoBehaviour
    {
        private SpriteRenderer _spriteRenderer;
        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            Resize();
        }

        private void Resize()
        {
            transform.position = SceneConsts.SceneCentre;
            float width = _spriteRenderer.bounds.size.x;
            float height = _spriteRenderer.bounds.size.y;
            float worldScreenHeight = SceneConsts.HalfHeight * 2f;
            float worldScreenWidth = SceneConsts.HalfWidth * 2f;
            transform.localScale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, 0);
        }
    }
}