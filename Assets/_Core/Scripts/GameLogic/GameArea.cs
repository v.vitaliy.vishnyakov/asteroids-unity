﻿using System;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

namespace _Core
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class GameArea : MonoBehaviour
    {
        private const float _teleportTolerance = 0.11F;
        private BoxCollider2D _areaCollider;

        private void Start()
        {
            Initialize();
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.TryGetComponent(out BulletBehaviour bullet))
            {
                bullet.DestroySelf();
                return;
            }
            if (other.gameObject.TryGetComponent(out ITeleportable obj))
            {
                Teleport(obj);
            }
        }

        private void Teleport(ITeleportable obj)
        {
            Vector2 objPosition = obj.GetObject().transform.position;
            Vector2 exitPoint = _areaCollider.ClosestPoint(objPosition);
            float distance = Vector2.Distance(exitPoint, objPosition);
            Vector2 teleportPoint = CalculateTeleportPoint(exitPoint, distance);
            obj.Teleport(teleportPoint);
        }
        
        private Vector2 CalculateTeleportPoint(Vector2 exitPoint, float distanceToExitPoint)
        {
            Vector2 teleportPoint = exitPoint;
            if (IsCrossedHorizontalBorder(exitPoint))
            {
                teleportPoint.y = exitPoint.y * -1;
                teleportPoint.y += distanceToExitPoint * Mathf.Sign(teleportPoint.y);
            }
            if (IsCrossedVerticalBorder(exitPoint))
            {
                teleportPoint.x = exitPoint.x * -1;
                teleportPoint.x += distanceToExitPoint * Mathf.Sign(teleportPoint.x);
            }
            return teleportPoint;
        }
        
        private bool IsCrossedHorizontalBorder(Vector2 exitPoint)
        {
            return Math.Abs(Math.Abs(exitPoint.y) - SceneConsts.HalfHeight) < _teleportTolerance;
        }
        private bool IsCrossedVerticalBorder(Vector2 exitPoint)
        {
            return Math.Abs(Math.Abs(exitPoint.x) - SceneConsts.HalfWidth) < _teleportTolerance;
        }
        
        private void Initialize()
        {
            _areaCollider = GetComponent<BoxCollider2D>();
            _areaCollider.size = new Vector2(SceneConsts.HalfWidth * 2, SceneConsts.HalfHeight * 2);
            transform.position = SceneConsts.SceneCentre;
        }
    }
}