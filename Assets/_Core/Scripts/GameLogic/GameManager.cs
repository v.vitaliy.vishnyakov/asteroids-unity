﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Core
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager _instance;
        [SerializeField] private GameParameters _parameters;
        private PlayerController _player;

        private bool _isGameRunning;
        private int _scoreMultiplier;
        private int _currentHp;
        private int _score;
        private float _xSpawnCoordinate;
        private float _ySpawnCoordinate;
        private List<Coroutine> _spawnRoutinesList;

        private void Awake()
        {
            CreateSingleton();
            InitializeManager();
        }
        
        private void CreateSingleton()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        private void InitializeManager()
        {
            _xSpawnCoordinate = SceneConsts.HalfWidth + SceneConsts.BorderOffset * 0.5F;
            _ySpawnCoordinate = SceneConsts.HalfHeight + SceneConsts.BorderOffset * 0.5F;
            _spawnRoutinesList = new List<Coroutine>();
        }
        
        private void Start()
        {
            InitializeAppOnStartUp();
        }

        private void InitializeAppOnStartUp()
        {
            UserManager.LoadGameData();
#if UNITY_EDITOR
            UIManager.HideScreen<GameHUD>();
            UIManager.HideAllScreens();
            ClearGameArea();
#endif
            UIManager.ShowScreen<MainMenuScreen>();
            InputManager.EnableUIControls();
            SoundManager.PlayMusic(_instance._parameters.MenuMusicKey, _instance._parameters.MenuMusicStartFadeTime);
        }

        public static void AddHp()
        {
            if (_instance._currentHp != _instance._parameters.MaxHp)
            {
                _instance._currentHp += 1;
                UIManager.SetHpHUD(_instance._currentHp);
            }
        }
        
        public static void TakeHp(int hp = 1)
        {
            _instance._currentHp -= hp;
            UIManager.SetHpHUD(_instance._currentHp);
            if (_instance._currentHp <= 0)
            {
                EndGame();
                return;
            }
            _instance._player.Revive(_instance._parameters.ReviveTime);
        }
        
        public static void AddScore(int number)
        {
            _instance._score += number*_instance._scoreMultiplier;
            UIManager.SetScoreHUD(_instance._score);
        }
        public static void SetScoreMultiplier(int scoreMultiplier)
        {
            if (_instance._scoreMultiplier != 1)
            {
                _instance.StopCoroutine(WaitForScoreMultiplierReset());
            }
            _instance._scoreMultiplier = scoreMultiplier;
            _instance.StartCoroutine(WaitForScoreMultiplierReset());
            UIManager.SetScoreMultiplierHUD(_instance._scoreMultiplier);
        }
        public static void ResetScoreMultiplier()
        {
            _instance._scoreMultiplier = 1;
            UIManager.ResetScoreMultiplierHUD();
        }
        private static IEnumerator WaitForScoreMultiplierReset()
        {
            yield return new WaitForSeconds(_instance._parameters.ScoreBoostDuration);
            ResetScoreMultiplier();
            yield return null;
        }

        public static void AddCredits(int amount)
        {
            UserManager.Credits += amount;
        }

        private static void InitializeGameSessionParameters()
        {
            _instance._currentHp = _instance._parameters.MaxHp;
            _instance._scoreMultiplier = 1;
            _instance._score = 0;
            _instance._player = SpawnManager.SpawnPlayer();
            _instance._player.transform.position = SceneConsts.SceneCentre;
            StartSpawnRoutines();
            _instance.SpawnAsteroids(_instance._parameters.InitialAsteroidsNumber);
        }
        private static void StartSpawnRoutines()
        {
            StartSpawnRoutine(
                Random.Range(_instance._parameters.AsteroidSpawnLowBorder, _instance._parameters.AsteroidSpawnHighBorder),
                _instance.SpawnAsteroid);
            StartSpawnRoutine(
                _instance._parameters.AlienSpawnCheckTime,
                _instance.SpawnAlien);
            StartSpawnRoutine(
                Random.Range(
                    _instance._parameters.BoostSpawnParameters.BoostSpawnMinTime,
                    _instance._parameters.BoostSpawnParameters.BoostSpawnMaxTime),
                _instance.SpawnBoost);
        }

        private static void StartSpawnRoutine(float time, Action action)
        {
            _instance._spawnRoutinesList.Add(_instance.StartCoroutine(_instance.SpawnRoutine(time, action)));
        }
        private IEnumerator SpawnRoutine(float time, Action spawnCallback)
        {
            while (_isGameRunning)
            {
                yield return new WaitForSeconds(time);
                {
                    spawnCallback?.Invoke();
                }    
            }
        }
        private static void StopSpawnRoutines()
        {
            foreach (Coroutine spawnRoutine in _instance._spawnRoutinesList)
            {
                _instance.StopCoroutine(spawnRoutine);
            }
        }
        private void SpawnAsteroid()
        {
            Vector2 spawnPosition = GetRandomPositionBeyond();
            Vector2 direction = Utils.GetDirectionOnPoint(spawnPosition, _player.transform.position);
            float speed = Random.Range
                (_parameters.AsteroidSpawnSpeedLowBorder, _parameters.AsteroidSpawnSpeedHighBorder);
            if (Utils.GetRandomBool())
            {
                SpawnManager.SpawnAsteroid<MidAsteroid>(spawnPosition, direction, speed);
            }
            else
            {
                SpawnManager.SpawnAsteroid<BigAsteroid>(spawnPosition, direction, speed);
            }
        }
        private void SpawnBoost()
        {
            if (_parameters.BoostSpawnParameters.HpBoostDropChance >= Random.value)
            {
                Vector2 position = GetRandomPositionInside();
                SpawnManager.SpawnBoost<HpBoost>(position);
            }
            if (_parameters.BoostSpawnParameters.ScoreBoostDropChance >= Random.value)
            {
                Vector2 position = GetRandomPositionInside();
                SpawnManager.SpawnBoost<ScoreBoost>(position);
            }
            if (_parameters.BoostSpawnParameters.CreditsBoostDropChance >= Random.value)
            {
                Vector2 position = GetRandomPositionInside();
                SpawnManager.SpawnBoost<CreditBoost>(position);
            }
        }
        private void SpawnAlien()
        {
            if (_parameters.AlienSpawnChance >= Random.value)
            {
                Vector2 position = GetRandomPositionBeyond();
                Alien alien = SpawnManager.SpawnAlien(position);
                alien.Initialize(_player.gameObject);
            }
        }
        private void SpawnAsteroids(int number)
        {
            for (int i = 0; i < number; i++)
            {
                SpawnAsteroid();
            }
        }
        
        public static void StartGame()
        {
            _instance._isGameRunning = true;
            ClearGameArea();
            UIManager.HideScreen<MainMenuScreen>();
            UIManager.ShowScreen<GameHUD>(new HudParameters(_instance._parameters.MaxHp, UserManager.PlayerName));
            InputManager.DisableUIControls();
            InputManager.EnablePlayerControls();
            InitializeGameSessionParameters();
            SoundManager.StopMusic();
            SoundManager.PlayMusic(_instance._parameters.GameplayMusicKey, _instance._parameters.GameplayMusicStartFadeTime);
        }
        private static void EndGame()
        {
            _instance._isGameRunning = false;
            StopSpawnRoutines();
            UserManager.UpdateLeaderboard(_instance._score);
            SpawnManager.DestroyObject(_instance._player);
            UIManager.HideScreen<GameHUD>();
            UIManager.ShowScreen<EndgameScreen>(new EndgameScreenParameters(UserManager.PlayerName, _instance._score));
            InputManager.DisablePlayerControls();
            InputManager.EnableUIControls();
            SoundManager.StopMusic(_instance._parameters.GameplayMusicEndFadeTime);
        }
        
        public static void GoToMainMenu()
        {
            ClearGameArea();
            UIManager.HideAllScreens();
            UIManager.ShowScreen<MainMenuScreen>();
            SoundManager.PlayMusic(_instance._parameters.MenuMusicKey, _instance._parameters.MenuMusicStartFadeTime);
        }
        
        public static void ClearGameArea()
        {
            SpawnManager.DestroyAllObjects();
        }
        
        private Vector2 GetRandomPositionBeyond()
        {
            Vector2 position = new Vector2();
            if (Utils.GetRandomBool())
            {
                position.x = Random.Range(-_xSpawnCoordinate, _xSpawnCoordinate);
                position.y = Utils.GetRandomBool() ? _ySpawnCoordinate : -_ySpawnCoordinate;
            }
            else
            {
                position.x = Utils.GetRandomBool() ? _xSpawnCoordinate : -_xSpawnCoordinate;
                position.y = Random.Range(-_ySpawnCoordinate, _ySpawnCoordinate);
            }
            return position;
        }

        public static Vector2 GetRandomPositionInside()
        {
            return new Vector2
            (Random.Range(-SceneConsts.HalfWidth, SceneConsts.HalfWidth),
                Random.Range(-SceneConsts.HalfHeight, SceneConsts.HalfHeight));
        }
        
        public static void SetNewPlayerModel(ShipModel shipModel)
        {
            SpawnManager.SetNewShipPrefab(shipModel.ship);
        }

        public static void ResetGameData()
        {
            UserManager.ResetUserData();
            SoundManager.ResetSoundData();
            ItemsManager.ResetItemsData();
        }

    }
}