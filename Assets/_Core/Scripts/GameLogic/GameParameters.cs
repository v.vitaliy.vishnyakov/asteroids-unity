﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "GameParameters", menuName = "Parameters/Game Parameters", order = 0)]
    public class GameParameters : ScriptableObject
    {
        [SerializeField] private int _maxHp;
        [SerializeField] private int _initialAsteroidsNumber;
        [SerializeField] private int _reviveTime;

        [Header("Spawn parameters")]
        [SerializeField] private BoostParameters _boostSpawnParameters;
        [SerializeField] private float _asteroidSpawnLowBorder;
        [SerializeField] private float _asteroidSpawnHighBorder;
        [SerializeField] private float _asteroidSpawnSpeedLowBorder;
        [SerializeField] private float _asteroidSpawnSpeedHighBorder;
        [SerializeField] private float _scoreBoostDuration;        
        [SerializeField] private float _alienSpawnCheckTime;
        [SerializeField] private float _alienSpawnChance;
        [Header("Data")]
        [SerializeField] private int _leaderboardMaxSize;
        [SerializeField] private string _defaultUserName;
        [Header("Sounds")]
        [SerializeField] private string _gameplayMusicKey;
        [SerializeField] private float _gameplayMusicStartFadeTime;
        [SerializeField] private float _gameplayMusicEndFadeTime;
        [SerializeField] private string _menuMusicKey;
        [SerializeField] private float _menuMusicStartFadeTime;
        [SerializeField] private float _menuMusicEndFadeTime;

        public int MaxHp => _maxHp;
        public int InitialAsteroidsNumber => _initialAsteroidsNumber;
        public int ReviveTime => _reviveTime;
        public BoostParameters BoostSpawnParameters => _boostSpawnParameters;
        public float ScoreBoostDuration => _scoreBoostDuration;
        public float AsteroidSpawnLowBorder => _asteroidSpawnLowBorder;
        public float AsteroidSpawnHighBorder => _asteroidSpawnHighBorder;
        public float AsteroidSpawnSpeedLowBorder => _asteroidSpawnSpeedLowBorder;
        public float AsteroidSpawnSpeedHighBorder => _asteroidSpawnSpeedHighBorder;
        public float AlienSpawnCheckTime => _alienSpawnCheckTime;
        public float AlienSpawnChance => _alienSpawnChance;
        public int LeaderboardMaxSize => _leaderboardMaxSize;
        public string DefaultUserName => _defaultUserName;
        public string GameplayMusicKey => _gameplayMusicKey;
        public string MenuMusicKey => _menuMusicKey;

        public float GameplayMusicStartFadeTime => _gameplayMusicStartFadeTime;

        public float GameplayMusicEndFadeTime => _gameplayMusicEndFadeTime;

        public float MenuMusicStartFadeTime => _menuMusicStartFadeTime;

        public float MenuMusicEndFadeTime => _menuMusicEndFadeTime;
    }
}
