﻿using UnityEngine;

namespace _Core
{
    public static class SceneConsts
    {
        public const float BorderOffset = 2.5F;
        public static readonly float HalfWidth = Camera.main.orthographicSize * Camera.main.aspect;
        public static readonly float HalfHeight= Camera.main.orthographicSize;
        public static readonly Vector2 SceneCentre = Camera.main.transform.position;
    }
}