﻿using UnityEngine;

namespace _Core
{
    public class InputManager : MonoBehaviour
    {
        private static InputManager _instance;
        private MyInput _myInput;
        private static MyInput.PlayerActions PlayerActions => _instance._myInput.Player;
        private static MyInput.UIActions UIActions => _instance._myInput.UI;
        
        private void Awake()
        {
            CreateSingleton();
            Initialize();
        }

        private void Initialize()
        {
            _myInput = new MyInput();
        }

        private void CreateSingleton()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        
        public static bool GetAccelerationValue()
        {
            return PlayerActions.Accelerate.ReadValue<float>()>0;
        }

        public static float GetRotationValue()
        {
            return PlayerActions.Rotate.ReadValue<float>();
        }
        
        public static bool GetShootValue()
        {
            return PlayerActions.Shoot.ReadValue<float>()>0;
        }

        public static bool GetCancelButtonValue()
        {
            return UIActions.Back.ReadValue<float>() > 0;
        }
        
        public static bool GetSubmitButtonValue()
        {
            
            return UIActions.Submit.ReadValue<float>() > 0;
        }

        public static void EnablePlayerControls()
        {
            _instance._myInput.Player.Enable();
        }
        
        public static void DisablePlayerControls()
        {
            _instance._myInput.Player.Disable();
        }
        
        public static void EnableUIControls()
        {
            _instance._myInput.UI.Enable();
        }
        
        public static void DisableUIControls()
        {
            _instance._myInput.UI.Disable();
        }
    }
}