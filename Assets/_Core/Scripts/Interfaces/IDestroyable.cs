﻿namespace _Core
{
    public interface IDestroyable
    {
        public void DestroySelf();
    }
}