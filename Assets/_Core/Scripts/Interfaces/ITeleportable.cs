﻿using UnityEngine;

namespace _Core
{
    public interface ITeleportable
    {
        void Teleport(Vector3 teleportPoint);
        GameObject GetObject();
    }
}