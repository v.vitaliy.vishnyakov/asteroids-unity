﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _Core
{
    public class PlayerController : MonoBehaviour, IDestroyable, ITeleportable,IPoolableObject
    {
        [SerializeField] private PlayerParameters _parameters;
        [SerializeField] private GameObject _splashObject;
        [SerializeField] private float _bulletSpawnPositionY;
        private bool _isInitialized;
        private bool _isAccelerating;
        private float _speed;
        private float _timeFromLastShot;
        private Vector2 _forwardDirection;
        private Vector2 _inertiaForce=Vector2.zero;
        private Vector2 _lastPosition;
        private SpriteRenderer _spriteRenderer;
        private LayerMask _hitMask;
        private Color _defaultColor;

        public Sprite ShipSkin
        {
            set => _spriteRenderer.sprite = value;
        }
        
        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnEnable()
        {
            Initialize();
        }

        public void Initialize()
        {
            HideSplash();
            MakeVulnerable();
            _defaultColor = _spriteRenderer.color;
            _isInitialized = true;
        }

        private void Update()
        {
            if (_isInitialized)
            {
                RotationInput();
                AccelerationInput();
                CalculateNextPosition();
                CalculateCurrentSpeed();
                ShootingInput();     
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (Utils.CheckCollision(other.gameObject, _hitMask))
            {
                TakeDamage();
            }
        }

        private void TakeDamage()
        {
            HideSplash();
            SoundManager.StopSFX(_parameters.SplashSoundKey, _parameters.SplashSoundFadeTime);
            SoundManager.PlaySFX(_parameters.CrashSound);
            GameManager.TakeHp();
        }

        private void RotationInput()
        {
            float rotateDirection = InputManager.GetRotationValue();
            if (rotateDirection != 0)
            {
                transform.rotation *= Quaternion.Euler
                    (0,0, rotateDirection * -1 * _parameters.RotateSpeed * Time.deltaTime);    
            }
            _forwardDirection = transform.rotation *  Vector2.up;
        }

        private void AccelerationInput()
        {
            if (InputManager.GetAccelerationValue())
            {
                Accelerate();
                SoundManager.PlaySFX(_parameters.SplashSoundKey);
            }
            else if (_isAccelerating)
            {
                StopAcceleration();
                SoundManager.StopSFX(_parameters.SplashSoundKey, _parameters.SplashSoundFadeTime);
            }    
        }
        private void Accelerate()
        {
            if (!_isAccelerating)
            {
                ShowSplash();
                _isAccelerating = true;
            }
            else
            {
                CalculateInertia();   
            }
        }
        private void CalculateInertia()
        {
            if (_inertiaForce.magnitude < _parameters.MaxSpeed)
            {
                _inertiaForce += _forwardDirection / _parameters.Mass * Time.deltaTime;
            }
        }
        private void StopAcceleration()
        {
            HideSplash();
            _isAccelerating = false;
        }
        
        private void CalculateNextPosition()
        {
            _lastPosition = transform.position;
            transform.position += (Vector3) _inertiaForce * Time.deltaTime;
            ReduceInertia();
        }
        private void ReduceInertia()
        {
            
            _inertiaForce /= (1+_parameters.Drag*Time.deltaTime);
            if (_inertiaForce.magnitude < 0.01)
            {
                _inertiaForce=Vector2.zero;
            }
        }
        
        private void CalculateCurrentSpeed()
        {
            float distance = Vector2.Distance(transform.position, _lastPosition);
            _speed = distance / Time.deltaTime;
        }
        
        private void ShootingInput()
        {
            if(InputManager.GetShootValue())
            {
                Shoot();
            }
        }
        private void Shoot()
        {
            _timeFromLastShot += Time.deltaTime;
            if (_timeFromLastShot >= _parameters.ShootCooldown)
            {
                CreateAndShootBullet();
                _timeFromLastShot = 0;
            }
        }
        
        private void CreateAndShootBullet()
        {
            Vector2 bulletPosition = transform.TransformPoint
                ( new Vector3(0, _bulletSpawnPositionY, 0));
            BulletBehaviour bullet = SpawnManager.SpawnBullet(bulletPosition);
            bullet.Initialize(_forwardDirection, _parameters.BulletParameters, _speed);
        }

        private void ShowSplash()
        {
            _splashObject.SetActive(true);
        }
        private void HideSplash()
        {
            _splashObject.SetActive(false);
        }

        public void DestroySelf()
        {
            SpawnManager.DestroyObject(this);
        }
        
        public void Teleport(Vector3 teleportPoint)
        {
            transform.position = teleportPoint;
        }
        
        public GameObject GetObject()
        {
            return gameObject;
        }

        public void Revive(int reviveTime)
        {
            StartCoroutine(ReviveRoutine(reviveTime));
        }

        private IEnumerator ReviveRoutine(int reviveTime)
        {
            transform.position = SceneConsts.SceneCentre;
            StopMovement();
            MakeInvincible();
            SetShipColor(_parameters.ColorOnRevive);
            yield return new WaitForSeconds(reviveTime);
            MakeVulnerable();
            SetShipColor(_defaultColor);
        }

        private void SetShipColor(Color newColor)
        {
            foreach (var splash in _splashObject.GetComponentsInChildren<SpriteRenderer>())
            {
                splash.color = newColor;
            }
            _spriteRenderer.color = newColor;
        }
        private void MakeInvincible()
        {
            _hitMask = 0;
        }
        private void MakeVulnerable()
        {
            _hitMask = _parameters.HitMask;
        }

        private void StopMovement()
        {
            _speed = 0;
            _lastPosition = Vector2.zero;
            _inertiaForce = Vector2.zero;
            _isAccelerating = false;
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }
        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        private void OnDisable()
        {
            _isInitialized = false;
            StopMovement();
        }
    }
}