﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "PlayerParameters", menuName = "Parameters/Player Parameters", order = 0)]
    public class PlayerParameters : ScriptableObject
    {
        [Header("Movement")]
        [SerializeField] private float maxSpeed = 5;
        [SerializeField] private float rotateSpeed = 160;
        [SerializeField] private float mass = 0.3F;
        [SerializeField] private float drag = 0.4F;
        [Header("Shooting")]
        [SerializeField] private float shootCooldown = 0.3F;
        [SerializeField] private BulletParameters bulletParameters;
        [Header("Sound")]
        [SerializeField] private string _splashSoundKey;
        [SerializeField] private float _splashSoundFadeTime;
        [SerializeField] private string _crashSound;
        [Header("Other")]
        [SerializeField] private LayerMask hitMask;
        [SerializeField] private Color colorOnRevive;
        
        public BulletParameters BulletParameters => bulletParameters;
        public float MaxSpeed => maxSpeed;
        public float RotateSpeed => rotateSpeed;
        public float ShootCooldown => shootCooldown;
        public float Mass => mass;
        public float Drag => drag;
        public LayerMask HitMask => hitMask;
        public Color ColorOnRevive => colorOnRevive;
        public string SplashSoundKey => _splashSoundKey;
        public float SplashSoundFadeTime => _splashSoundFadeTime;

        public string CrashSound => _crashSound;
    }
}