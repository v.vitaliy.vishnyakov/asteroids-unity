﻿using System;
using System.Collections.Generic;

namespace _Core
{
    [Serializable]
    public class ItemsData
    {
        public List<int> _purchasedItemsIds;
        public int _chosenItemId;

        public ItemsData() {}

        public ItemsData(List<int> purchasedItemsIds, int chosenItemId)
        {
            _purchasedItemsIds = purchasedItemsIds;
            _chosenItemId = chosenItemId;
        }
    }
}