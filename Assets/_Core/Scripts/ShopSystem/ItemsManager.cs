﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    public class ItemsManager : MonoBehaviour
    {
        private static ItemsManager _instance;
        private const string ItemsDataKey = "ItemsDataKey";
        private const string ItemsStorageFileName = "ItemsStorage";
        private ItemsStorage _itemsStorage;
        private ItemsData _itemsData;
        [SerializeField] private int _itemByDefaultId; 
        
        public static List<ShopItem> Items => _instance._itemsStorage.Items;
        public static int ChosenItemId => _instance._itemsData._chosenItemId;
        
        private void Awake()
        {
            CreateSingleton();
        }
        
        private void CreateSingleton()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        
        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            LoadFullItemsData();
            ApplyPreviouslyChosenItem();
        }

        private void LoadFullItemsData()
        {
            _itemsStorage = Resources.Load<ItemsStorage>(ItemsStorageFileName); 
            _itemsData = LoadItemsData();
        }

        private void ApplyPreviouslyChosenItem()
        {
            ApplyItem(_itemsData._chosenItemId);
        }
        
        public static void ApplyItem(int itemId)
        {
            ShopItem shopShopItem = _instance._itemsStorage.Items.Find(x => x.Id == itemId);
            if (shopShopItem is ShipShopItem shipItem)
            {
                GameManager.SetNewPlayerModel(shipItem.Data as ShipModel);
            }
            _instance._itemsData._chosenItemId = itemId;
            _instance.SaveItemsData();
        }

        public static bool TryToBuyItem(IShopItemArgs itemArgs)
        {
            if (UserManager.TrySpendCredits(itemArgs.Price))
            {
                _instance._itemsData._purchasedItemsIds.Add(itemArgs.Id);
                _instance.SaveItemsData();
                return true;
            }
            return false;
        }

        public static bool IsItemPurchased(int itemId)
        {
            return _instance._itemsData._purchasedItemsIds.Contains(itemId);
        }
        
        private ItemsData LoadItemsData()
        {
            ItemsData loadedItemsData = SaveManager.Load<ItemsData>(ItemsDataKey);
            if (loadedItemsData == null)
            {
                loadedItemsData = new ItemsData()
                {
                    _purchasedItemsIds = new List<int>(new int[]{_itemByDefaultId}),
                    _chosenItemId = _itemByDefaultId
                };
            }
            return loadedItemsData;
        }

        private void SaveItemsData()
        {
            SaveManager.Save(ItemsDataKey, _instance._itemsData);
        }

        public static void ResetItemsData()
        {
            SaveManager.Delete<ItemsData>(ItemsDataKey);
            _instance.Initialize();
            UIManager.ForceInitializeShopScreen();
        }
    }
}