﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "PlayerModel", menuName = "Shop/Player Model", order = 2)]
    public class ShipModel : ScriptableObject, IShopItemData
    {
        public PlayerController ship;
    }
}