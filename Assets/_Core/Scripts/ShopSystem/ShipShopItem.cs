﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "ShipShopItem", menuName = "Shop/Ship Shop Item", order = 0)]
    public class ShipShopItem : ShopItem
    {
        [SerializeField] private int id;
        [SerializeField] private int price;
        [SerializeField] private string itemName;
        [SerializeField] private Sprite itemSprite;
        [SerializeField] private ShipModel shipModel;

        public override int Id => id;
        public override int Price => price;
        public override string Name => itemName;
        public override Sprite Image => itemSprite;
        public override IShopItemData Data => shipModel;
    }
}