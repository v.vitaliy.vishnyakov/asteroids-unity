﻿namespace _Core
{
    public class ShopItemArgs : IShopItemArgs
    {
        public int Id { get; }
        public int Price { get; }
        
        public ShopItemArgs(ShopItem shipShopItem)
        {
            Id = shipShopItem.Id;
            Price = shipShopItem.Price;
        }
    }
}