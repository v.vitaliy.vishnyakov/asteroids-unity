﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class ShopItemView : MonoBehaviour, IPoolableObject
    {
        [SerializeField] private Text priceLabel;
        [SerializeField] private Text nameLabel;
        [SerializeField] private Image viewImage;
        [SerializeField] private Button buyButton;
        [SerializeField] private Toggle chooseToggle;
        private Action<IShopItemArgs> _onChooseCallback;
        private Func<IShopItemArgs, bool> _onBuyCallback;
        private ShopItemArgs _shopItemArgs; 
        
        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            buyButton.onClick.AddListener(OnBuyButtonClick);
            chooseToggle.onValueChanged.AddListener(OnChooseToggleValueChanged);
        }

        public void InitializeByItem(ShopItem shopItem, bool isPurchased, bool isChosen, Func<IShopItemArgs, bool> onBuyCallback, Action<IShopItemArgs> onChooseCallback)
        {
            _shopItemArgs = new ShopItemArgs(shopItem);
            priceLabel.text = shopItem.Price.ToString();
            nameLabel.text = shopItem.Name;
            viewImage.sprite = shopItem.Image;
            _onChooseCallback = onChooseCallback;
            _onBuyCallback = onBuyCallback;
            MakeChoosable(isPurchased);
            if (isChosen)
            {
                chooseToggle.SetIsOnWithoutNotify(true);
            }
        }

        private void OnBuyButtonClick()
        {
            if (_onBuyCallback(_shopItemArgs))
            {
                MakeChoosable(true);
            }
        }

        private void OnChooseToggleValueChanged(bool value)
        {
            if (value)
            {
                _onChooseCallback(_shopItemArgs);    
            }
        }

        public void AddToToggleGroup(ToggleGroup toggleGroup)
        {
            chooseToggle.group = toggleGroup;
        }
        
        private void MakeChoosable(bool value)
        {
            buyButton.gameObject.SetActive(!value);
            priceLabel.gameObject.SetActive(!value);
            chooseToggle.gameObject.SetActive(value);
        }
        
        public void Enable()
        {
            gameObject.SetActive(true);
            
        }
        
        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }
    }
}