﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class ShopScreen : Screen
    {
        [Header("UI Elements")]
        [SerializeField] private Button _closeButton;
        [SerializeField] private Text _creditsAmountLabel;
        [SerializeField] private RectTransform _itemsPanel;
        [SerializeField] private ToggleGroup _toggleGroup;
        [Header("Prefabs")]
        [SerializeField] private ShopItemView _itemViewPrefab;
        private bool _isInitialized;
        private List<ShopItemView> _itemViews = new List<ShopItemView>();
        
        private void Awake()
        {
            InitializeScreen();
        }
        private void InitializeScreen()
        {
            _closeButton.onClick.AddListener(Hide);
        }
        private void OnEnable()    
        {
            if (!_isInitialized)
            {
                InitializeShop();
                _isInitialized = true;
            }
        }

        private void InitializeShop()
        {
            List<ShopItem> shopItems = ItemsManager.Items;
            if (shopItems != null)
            {
                foreach (var item in shopItems)
                {
                    var itemView = SpawnManager.SpawnShopItemView();
                    itemView.gameObject.transform.SetParent(_itemsPanel);
                    bool isItemPurchased = ItemsManager.IsItemPurchased(item.Id);
                    bool isChosen = ItemsManager.ChosenItemId == item.Id;
                    itemView.InitializeByItem(item, isItemPurchased, isChosen, OnBuyingItem, OnChoosingItem);
                    itemView.AddToToggleGroup(_toggleGroup);
                    _itemViews.Add(itemView);
                }    
            }
        }

        public void ReInitializeShop()
        {
            foreach (var shopItemView in _itemViews)
            {
                shopItemView.Disable();
            }
            _itemViews.Clear();
            _isInitialized = false;
        }

        public override void Show(IScreenParameters screenParameters)
        {
            _creditsAmountLabel.text = UserManager.Credits.ToString();
            Show();
        }

        private void OnDisable()
        {
            UIManager.ShowScreen<MainMenuScreen>();
        }

        public void OnChoosingItem(IShopItemArgs itemArgs)
        {
            ItemsManager.ApplyItem(itemArgs.Id);
        }

        public bool OnBuyingItem(IShopItemArgs itemArgs)
        {
            if (ItemsManager.TryToBuyItem(itemArgs))
            {
                _creditsAmountLabel.text = UserManager.Credits.ToString();
                return true;
            }
            return false;
        }
    }
}