﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    [RequireComponent(typeof(Toggle))]
    public class MyToggle : MonoBehaviour
    {
        [SerializeField] private Toggle _toggle;
        private Action _onEnableCallback;
        private Action _onDisableCallback;

        private void Awake()
        {
            _toggle.GetComponent<Toggle>();
        }

        public void Initialize(bool initialValue, Action onEnableCallback, Action onDisableCallback)
        {
            _toggle.SetIsOnWithoutNotify(initialValue);
            _toggle.onValueChanged.AddListener(OnToggleValueChange);
            _onEnableCallback = onEnableCallback;
            _onDisableCallback = onDisableCallback;
        }
        
        private void OnToggleValueChange(bool value)
        {
            if (value)
            {
                _onEnableCallback?.Invoke();
            }
            else
            {
                _onDisableCallback?.Invoke();
            }
        }

        public void SetValue(bool value)
        {
            _toggle.isOn = value;
        }
    }
}