﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Core
{
    public class SpawnManager : MonoBehaviour
    {
        private static SpawnManager _instance = null;
        
        [SerializeField] private int _tinyAsteroidsToPool; 
        [SerializeField] private int _midAsteroidsToPool;
        [SerializeField] private int _bigAsteroidsToPool;
        [SerializeField] private int _bulletsToPool;
        [SerializeField] private SpawnPrefabs _prefabs;

        private List<Asteroid> _asteroidsPool = new List<Asteroid>();
        private List<BulletBehaviour> _bulletsPool = new List<BulletBehaviour>();
        private List<Boost> _boostsPool = new List<Boost>();
        private List<Alien> _alienPool = new List<Alien>();
        private List<RecordString> _recordStringsPool = new List<RecordString>();
        private List<ShopItemView> _shopItemViewsPool = new List<ShopItemView>();
        private List<Sound> _soundsPool = new List<Sound>();
        private PlayerController _player;

        private void Awake ()
        {
            CreateSingleton();
            InitializeManager();
        }

        private void CreateSingleton()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        private void InitializeManager()
        {
            FillAsteroidsPool<BigAsteroid>(_bigAsteroidsToPool);
            FillAsteroidsPool<MidAsteroid>(_midAsteroidsToPool);
            FillAsteroidsPool<TinyAsteroid>(_tinyAsteroidsToPool);
            FillBulletsPool();
        }

        public static PlayerController SpawnPlayer()
        {
            if (_instance._player == null)
            {
                _instance._player = Instantiate(_instance._prefabs.player);
                _instance._player.Disable();
            }
            _instance._player.Enable();
            return _instance._player;
        }

        private void FillAsteroidsPool<T>(int asteroidsToPool) where T: Asteroid
        {
            List<Asteroid> prefabs = _prefabs.asteroidsPrefabs.Where(a => a is T).ToList(); 
            int j = 0;
            for (int i = 0; i<asteroidsToPool; i++)
            {
                Asteroid asteroid = Instantiate(prefabs[j]);
                asteroid.gameObject.SetActive(false);
                _asteroidsPool.Add(asteroid);
                j++;
                if (j == prefabs.Count)
                {
                    j = 0;
                }
            }
        }
        private void FillBulletsPool()
        {
            for (int i = 0; i <= _bulletsToPool-1; i++)
            {
                BulletBehaviour bullet = Instantiate(_prefabs.bulletPrefab);
                bullet.gameObject.SetActive(false);
                _bulletsPool.Add(bullet);
            }
        }
        
        public static Asteroid SpawnAsteroid<T>(Vector2 position, Vector2 direction, float speed) where T: Asteroid
        {
           Asteroid asteroid = _instance.Spawn<Asteroid, T>(position, _instance._asteroidsPool, _instance.GetAsteroidPrefab<T>());
           asteroid.Initialize(direction,speed);
           return asteroid;
        }
        private Asteroid GetAsteroidPrefab<T>() where T : Asteroid
        {
            var prefabsList = _prefabs.asteroidsPrefabs.Where(a => a is T).ToList();
            return prefabsList[Random.Range(0, prefabsList.Count - 1)];
        }
        
        public static Boost SpawnBoost<T>(Vector2 position) where T : Boost, IPoolableObject
        {
            return _instance.Spawn<Boost, T>(position, _instance._boostsPool, _instance.GetBoostPrefab<T>());
        }

        private Boost GetBoostPrefab<T>() where T : Boost
        {
            return _prefabs.boostsPrefabs.FirstOrDefault(b => b is T);
        }
        
        public static BulletBehaviour SpawnBullet(Vector2 position)
        {
            return _instance.Spawn(position, _instance._bulletsPool, _instance._prefabs.bulletPrefab);
        }
        
        public static Alien SpawnAlien(Vector2 position)
        {
            return _instance.Spawn(position, _instance._alienPool, _instance._prefabs.alien);
        }
        
        public static RecordString SpawnRecordString()
        {
            return _instance.Spawn(Vector2.zero, _instance._recordStringsPool, _instance._prefabs.recordString);
        }
        public static ShopItemView SpawnShopItemView()
        {
            return _instance.Spawn(Vector2.zero, _instance._shopItemViewsPool, _instance._prefabs.shopItemView);
        }

        public static Sound SpawnSound()
        {
            return _instance.Spawn(Vector2.zero, _instance._soundsPool, _instance._prefabs.soundPrefab);
        }
        
        private T Spawn<T, U>(Vector2 position, List<T> pool, T prefab) 
            where T: MonoBehaviour, IPoolableObject
            where U: T
        {
            T obj = null;
            foreach (var poolObj in pool)
            {
                if (poolObj is U && !poolObj.gameObject.activeInHierarchy)
                {
                    obj = poolObj;
                    break;
                }
            }
            if (obj == null)
            {
                obj = Instantiate(prefab);
                pool.Add(obj);
            }
            obj.gameObject.transform.position = position;
            obj.Enable();
            return obj;       
        }
        private T Spawn<T>(Vector2 position, List<T> pool, T prefab) where T : MonoBehaviour, IPoolableObject
        {
            return Spawn<T, T>(position, pool, prefab);
        }
        
        public static void DestroyObject(IPoolableObject gameObject)
        {
            gameObject.Disable();
        }

        public static void DestroyAllObjects()
        {
            DisableAllObjectsInPool(_instance._asteroidsPool);
            DisableAllObjectsInPool(_instance._boostsPool);
            DisableAllObjectsInPool(_instance._alienPool);
            DisableAllObjectsInPool(_instance._bulletsPool);
        }
        
        public static void DestroyAllRecordStrings()
        {
            DisableAllObjectsInPool(_instance._recordStringsPool);
        }

        public static void SetNewShipPrefab(PlayerController shipPrefab)
        {
            _instance._prefabs.player = shipPrefab;
            Destroy(_instance._player);
        }
        
        private static void DisableAllObjectsInPool<T>(List<T> pool) where T: IPoolableObject
        {
            foreach (var obj in pool)
            {
                if (obj.IsActive())
                {
                    obj.Disable();
                }
            }
        }
    }
}