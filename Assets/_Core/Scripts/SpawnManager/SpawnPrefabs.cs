﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    [Serializable]
    public class SpawnPrefabs
    {
        [Header("Objects")]
        public PlayerController player;
        public Alien alien;
        public BulletBehaviour bulletPrefab;
        public List<Asteroid> asteroidsPrefabs;
        public List<Boost> boostsPrefabs;
        [Header("UI Elements")]
        public RecordString recordString;
        public ShopItemView shopItemView;
        [Header("Sound")] 
        public Sound soundPrefab;

    }
}