﻿using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class RecordString : MonoBehaviour, IPoolableObject
    {
        [SerializeField] private Text _number;
        [SerializeField] private Text _name;
        [SerializeField] private Text _score;

        public void Initialize(string number, string name, string score)
        {
            _number.text = number;
            _name.text = name;
            _score.text = score;
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }
    }
}