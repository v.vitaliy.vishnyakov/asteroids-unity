﻿namespace _Core
{
    public class EndgameScreenParameters : IScreenParameters
    {
        public string NameToDisplay { get; }
        public int ScoreToDisplay { get; }

        public EndgameScreenParameters(string nameToDisplay, int scoreToDisplay)
        {
            NameToDisplay = nameToDisplay;
            ScoreToDisplay = scoreToDisplay;
        }
    }
}