﻿namespace _Core
{
    public class HudParameters : IScreenParameters
    {
        public int MaxHp { get; }
        public string PlayerName { get; }

        public HudParameters(int maxHp, string playerName)
        {
            MaxHp = maxHp;
            PlayerName = playerName;
        }
    }
}