﻿namespace _Core
{
    public class LeaderboardScreenParameters : IScreenParameters
    {
        public Leaderboard Leaderboard { get; }

        public LeaderboardScreenParameters(Leaderboard leaderboard)
        {
            Leaderboard = leaderboard;
        }
    }
}