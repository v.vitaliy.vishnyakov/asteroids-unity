﻿using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class ChangeNameScreen : Screen
    {
        [SerializeField] private Button _cancelButton;
        [SerializeField] private Button _applyButton;
        [SerializeField] private InputField _nameInputField;
        private string _placeHolderText;
        
        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            _cancelButton.onClick.AddListener(OnClickCancelButton);
            _applyButton.onClick.AddListener(OnClickApplyButton);
            _placeHolderText = _nameInputField.placeholder.GetComponent<Text>().text;
        }

        private void Update()
        {
            CheckKeysKeys();
        }

        private void CheckKeysKeys()
        {
            if (InputManager.GetSubmitButtonValue() && _nameInputField.isFocused)
            {
                _nameInputField.DeactivateInputField();
            }
            if (InputManager.GetCancelButtonValue())
            {
                _cancelButton.onClick.Invoke();
            }
        }

        private void OnEnable()
        {
            _nameInputField.text = _placeHolderText;
            _nameInputField.ActivateInputField();
        }

        private void OnDisable()
        {
            UIManager.ShowScreen<MainMenuScreen>();
        }

        private void OnClickApplyButton()
        {
            UserManager.PlayerName = _nameInputField.text;
            Hide();
        }
        
        private void OnClickCancelButton()
        {
            Hide();
        }

        public override void Show(IScreenParameters screenParameters)
        {
            Show();
        }
    }
}