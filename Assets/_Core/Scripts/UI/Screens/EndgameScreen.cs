﻿using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class EndgameScreen : Screen
    {
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _toMenuButton;
        [SerializeField] private Text _playerNameLabel;
        [SerializeField] private Text _scoreLabel;

        private void Awake()
        {
            Initialize();
        }
        
        private void Initialize()
        {
            _restartButton.onClick.AddListener(OnClickRestartButton);
            _toMenuButton.onClick.AddListener(OnClickToMenuButton);
        }
        
        public override void Show(IScreenParameters parameters)
        {
            if (parameters is EndgameScreenParameters screenParameters)
            {
                _playerNameLabel.text = screenParameters.NameToDisplay;
                _scoreLabel.text = "Score: " + screenParameters.ScoreToDisplay;
            }
            Show();
        }

        private void OnClickRestartButton()
        {
            Hide();
            GameManager.StartGame();
        }

        private void OnClickToMenuButton()
        {
            GameManager.GoToMainMenu();
        }
    }
}