﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class GameHUD: Screen
    {
        [SerializeField] private Text _scoreLabel;
        [SerializeField] private Text scoreMultiplierLabel;
        [SerializeField] private GameObject _hpBar;
        [SerializeField] private GameObject _hpCell;
        [SerializeField] private Text _playerNameLabel;
        [SerializeField] private GameObject _screenControls;
        private List<GameObject> _hpCells;
        public override void Show(IScreenParameters screenParameters)
        {
            if (screenParameters is HudParameters parameters)
            {
                Initialize(parameters.MaxHp, parameters.PlayerName);
            }
            Show();
        }
        
        public void Initialize(int maxHp, string playerName)
        {
            SetScore(0);
            ResetScoreMultiplier();
            SetupHpBar(maxHp);
            SetHp(maxHp);
            _playerNameLabel.text = playerName;
            #if UNITY_STANDALONE
            DisableOnScreenControls();
            #endif
            #if UNITY_ANDROID
            EnableOnScreenControls();            
            #endif
        }
        public void SetScore(int score)
        {
            _scoreLabel.text = "Score: " + score;
        }
        
        public void SetScoreMultiplier(int scoreMultiplier)
        {
            scoreMultiplierLabel.text = "x" + scoreMultiplier;
            scoreMultiplierLabel.gameObject.SetActive(true);
        }
        
        public void ResetScoreMultiplier()
        {
            scoreMultiplierLabel.gameObject.SetActive(false);
        }

        public void SetupHpBar(int maxHp)
        {
            _hpCells = new List<GameObject>();
            RectTransform cellRect = _hpCell.GetComponent<Image>().rectTransform;
            for (int i = 0; i < maxHp; i++)
            {
                GameObject hpCell = Instantiate(_hpCell, _hpBar.transform, false);
                _hpCells.Add(hpCell);
            }
        }
        public void SetHp(int hpNumber)
        {
            for (int i = 0; i < _hpCells.Count; i++)
            {
                _hpCells[i].gameObject.SetActive(i < hpNumber);
            }
        }

        public void EnableOnScreenControls()
        {
            _screenControls.SetActive(true);
        }
        public void DisableOnScreenControls()
        {
            _screenControls.SetActive(false);
        }
    }
}