﻿using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class LeaderboardScreen : Screen
    {
        [SerializeField] private RectTransform _recordsPanel;
        [SerializeField] private Text _noRecordsLabel;
        [SerializeField] private Button _closeButton;
        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            _closeButton.onClick.AddListener(Hide);
            _noRecordsLabel.enabled = false;
        }

        private void Update()
        {
            CheckKeys();
        }

        private void CheckKeys()
        {
            if (InputManager.GetCancelButtonValue())
            {
                _closeButton.onClick.Invoke();
            }
        }

        public override void Show(IScreenParameters parameters)
        {
            Show();
            if (parameters is LeaderboardScreenParameters screenParameters)
            {
                DrawRecords(screenParameters.Leaderboard);
            }
        }

        private void OnDisable()
        {
            UIManager.ShowScreen<MainMenuScreen>();
        }

        private void DrawRecords(Leaderboard leaderboard)
        {
            SpawnManager.DestroyAllRecordStrings();
            if (leaderboard.Records.Count == 0)
            {
                _noRecordsLabel.enabled = true;
                return;
            }
            _noRecordsLabel.enabled = false;
            for (int i = 0; i < leaderboard.Records.Count; i++)
            {
                RecordString recordString = SpawnManager.SpawnRecordString();
                recordString.gameObject.transform.SetParent(_recordsPanel);
                recordString.Initialize((i+1).ToString(), leaderboard.Records[i].name, leaderboard.Records[i].score.ToString());
            }
        }
    }
}