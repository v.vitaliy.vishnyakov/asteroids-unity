﻿using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class MainMenuScreen : Screen
    {
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _leaderboardButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _quitButton;
        [SerializeField] private Button _changeNameButton;
        [SerializeField] private Button _shopScreen;
        [SerializeField] private Text _playerNameLabel;
        
        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            _playButton.onClick.AddListener(OnClickPlayButton);
            _leaderboardButton.onClick.AddListener(OnClickLeaderboardButton);
            _settingsButton.onClick.AddListener(OnClickSettingsButton);
            _quitButton.onClick.AddListener(OnClickQuitButton);
            _changeNameButton.onClick.AddListener(OnClickChangeNameButton);
            _shopScreen.onClick.AddListener(OnClickShopButton);
        }
        
        private void OnClickChangeNameButton()
        {
            Hide();
            UIManager.ShowScreen<ChangeNameScreen>();
        }
        private void OnClickPlayButton()
        {
            GameManager.StartGame();
        }
        private void OnClickLeaderboardButton()
        {
            Hide();
            UIManager.ShowScreen<LeaderboardScreen>(new LeaderboardScreenParameters(UserManager.Leaderboard));
        }
        private void OnClickSettingsButton()
        {
            Hide();
            UIManager.ShowScreen<SettingsScreen>();
        }
        private void OnClickQuitButton()
        {
            Application.Quit();
        }

        private void OnClickShopButton()
        {
            Hide();
            UIManager.ShowScreen<ShopScreen>();
        }

        public override void Show(IScreenParameters screenParameters)
        {
            UpdatePlayerNameLabel(UserManager.PlayerName);
            Show();
        }
        
        public void UpdatePlayerNameLabel(string name)
        {
            _playerNameLabel.text = name;
        }
    }
}