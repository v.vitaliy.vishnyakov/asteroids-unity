﻿using UnityEngine;

namespace _Core
{
    public abstract class Screen : MonoBehaviour
    {
        public void Show()
        {
            gameObject.SetActive(true);
        }
        
        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public abstract void Show(IScreenParameters screenParameters);
    }
}