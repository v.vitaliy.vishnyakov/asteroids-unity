﻿using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class SettingsScreen : Screen
    {
        [SerializeField] private MyToggle _muteAllToggle;
        [SerializeField] private MyToggle _muteMusicToggle;
        [SerializeField] private MyToggle _muteSFXToggle;
        [SerializeField] private Button _closeButton;
        [SerializeField] private Button _resetButton;
        private bool _isInitialized;
        
        private void OnEnable()    
        {
            if (!_isInitialized)
            {
                Initialize();
                _isInitialized = true;
            }
        }

        private void Initialize()
        { 
            _muteAllToggle.Initialize
                (SoundManager.GlobalVolume == 0, SoundManager.MuteGlobalSound, SoundManager.EnableGlobalSound);
            _muteMusicToggle.Initialize
                (SoundManager.MusicVolume == 0, SoundManager.MuteMusic, SoundManager.EnableMusic);
            _muteSFXToggle.Initialize
                (SoundManager.SFXVolume == 0, SoundManager.MuteSFX, SoundManager.EnableSFX);
            _closeButton.onClick.AddListener(Hide);
            _resetButton.onClick.AddListener(OnResetButtonClick);
        }

        public override void Show(IScreenParameters screenParameters)
        {
            Show();
        }

        private void OnResetButtonClick()
        {
            GameManager.ResetGameData();
        }
        
        private void OnDisable()
        {
            UIManager.ShowScreen<MainMenuScreen>();
        }

        public void UpdateAllToggles()
        {
            _muteAllToggle.SetValue(SoundManager.GlobalVolume == 0);
            _muteMusicToggle.SetValue(SoundManager.MusicVolume == 0);
            _muteSFXToggle.SetValue(SoundManager.SFXVolume == 0);
        }
    }
}