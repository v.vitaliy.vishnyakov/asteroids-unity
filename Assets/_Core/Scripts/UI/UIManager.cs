﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class UIManager : MonoBehaviour
    {
        private static UIManager _instance;
        [SerializeField] private List<Screen> _screens;
        private GameHUD _hud; 
        private void Awake()
        {
            CreateSingleton();
            InitializeManager();
        }

        private void CreateSingleton()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        private void InitializeManager()
        {
            foreach (Screen screen in _screens)
            {
                if (screen is GameHUD hud)
                {
                    _hud = hud;
                    return;
                }
            }
        }
        
        public static void SetScoreHUD(int score)
        {
            _instance._hud.SetScore(score);
        }
        public static void SetScoreMultiplierHUD(int scoreMultiplier)
        {
            _instance._hud.SetScoreMultiplier(scoreMultiplier);
        }
        public static void ResetScoreMultiplierHUD()
        {
            _instance._hud.ResetScoreMultiplier();
        }
        public static void SetHpHUD(int hpNumber)
        {
            _instance._hud.SetHp(hpNumber);
        }
        
        public static void ShowScreen<T>(IScreenParameters screenParameters = null) where T : Screen
        {
            foreach (Screen screen in _instance._screens)
            {
                if (screen is T)
                {
                    screen.Show(screenParameters);
                    break;
                }
            }
        }

        public static void HideScreen<T>() where T : Screen
        {
            foreach (Screen screen in _instance._screens)
            {
                if (screen is T)
                {
                    screen.Hide();
                }
            }
        }
        public static void HideAllScreens()
        {
            foreach (Screen screen in _instance._screens)
            {
                screen.Hide();
            }
        }

        public static void ForceInitializeShopScreen()
        {
            foreach (Screen screen in _instance._screens)
            {
                if (screen is ShopScreen shopScreen)
                {
                    shopScreen.ReInitializeShop();
                }
            }
        }

        public static void UpdateSettingsScreen()
        {
            foreach (Screen screen in _instance._screens)
            {
                if (screen is SettingsScreen settingsScreen)
                {
                    settingsScreen.UpdateAllToggles();
                }
            }
        }

    }
}