﻿using System;
using System.Collections.Generic;

namespace _Core
{
    [Serializable]
    public class Leaderboard
    {
        public int maxSize;
        public List<Record> recordsList = new List<Record>();
        public List<Record> Records => recordsList;
        
        public Leaderboard(int maxSize)
        {
            this.maxSize = maxSize;
        }
        
        public bool TryAddNewRecord(Record newRecord)
        {
            int index = recordsList.FindIndex(x => x.name == newRecord.name);
            if (index != -1)
            {
                if (recordsList[index].score >= newRecord.score)
                {
                    return false;
                }
                recordsList[index].score = newRecord.score;
            }
            else
            {
                if (recordsList.Count == maxSize)
                {
                    recordsList.RemoveAt(recordsList.Count - 1);    
                }
                recordsList.Add(newRecord);
            }
            SortAscending();
            return true;
        }
        
        public void SortAscending()
        {
            recordsList.Sort((x, y)  => y.score.CompareTo(x.score));
        }
    }
}