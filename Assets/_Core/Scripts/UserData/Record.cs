﻿using System;

namespace _Core
{
    [Serializable]
    public class Record
    {
        public string name;
        public int score;
        public Record(string name, int score)
        {
            this.name = name;
            this.score = score;
        }
    }
}