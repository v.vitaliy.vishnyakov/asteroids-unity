﻿using UnityEngine;

namespace _Core
{
    public class SaveManager : MonoBehaviour
    {
        private static SaveManager _instance = null;
        private ISaveController _saveController;

        private void Awake()
        {
            CreateSingleton();
            InitializeManager();
        }
        
        private void CreateSingleton()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        
        private void InitializeManager()
        {
            _saveController = new PlayerPrefsSaveController();
        }

        public static void Save<T>(string key, T data)
        {
            _instance._saveController.Save(key, data);
        }
        public static T Load<T>(string key)
        {
            T smth = _instance._saveController.Load<T>(key);
            return smth;
        }
        public static void Delete<T>(string key)
        {
            _instance._saveController.Delete<T>(key);
        }
    }
}