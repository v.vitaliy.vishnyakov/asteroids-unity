﻿using System;

namespace _Core
{
    [Serializable]
    public class UserData
    {
        public int credits;
        public string playerName;
        public Leaderboard leaderboard;
        public ShipModel shipModel;
    }
}