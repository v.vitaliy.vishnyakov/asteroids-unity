﻿using UnityEngine;

namespace _Core
{
    public class UserManager : MonoBehaviour
    {
        private static UserManager _instance;
        private const string GameDataSaveKey = "GameData";
        [SerializeField] private GameParameters _parameters;
        private UserData _userData;
        
        public static Leaderboard Leaderboard
        {
            get => _instance._userData.leaderboard;
            private set => _instance._userData.leaderboard = value;
        }
        public static string PlayerName
        {
            get => _instance._userData.playerName;
            set => _instance._userData.playerName = value;
        }
        public static int Credits
        {
            get => _instance._userData.credits;
            set => _instance._userData.credits = value;
        }

        private void Awake()
        {
            CreateSingleton();
        }
        
        private void CreateSingleton()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        public static void SaveGameData()
        {
            SaveManager.Save(GameDataSaveKey, _instance._userData);
        }
        
        public static void LoadGameData()
        {
            _instance._userData = SaveManager.Load<UserData>(GameDataSaveKey);
            if (_instance._userData == null)
            {
                _instance._userData = new UserData
                {
                    leaderboard = new Leaderboard(_instance._parameters.LeaderboardMaxSize),
                    playerName = _instance._parameters.DefaultUserName,
                    credits = 0
                };
            }
        }
        
        public static void UpdateLeaderboard(int score)
        {
            if (Leaderboard.TryAddNewRecord(new Record(PlayerName, score)))
            {
                SaveGameData();
            }
        }

        public static bool TrySpendCredits(int creditAmount)
        {
            if (Credits >= creditAmount)
            {
                Credits -= creditAmount;
                SaveGameData();
                return true;
            }

            return false;
        }

        public static void ResetUserData()
        {
            SaveManager.Delete<UserData>(GameDataSaveKey);
            LoadGameData();
        }
    }
}