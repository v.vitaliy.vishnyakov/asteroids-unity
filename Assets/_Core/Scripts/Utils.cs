﻿using UnityEngine;

namespace _Core
{
    public class Utils
    {
        public static bool CheckCollision(GameObject collisionObject, LayerMask collisionMask)
        {
            int mask = 1 << collisionObject.layer;
            return (collisionMask.value & mask) != 0;
        }
        
        public static bool GetRandomBool()
        {
            return !(Random.Range(-2F, 1F) < 0);
        }

        public static Vector2 GetDirectionOnPoint(Vector2 fromPosition, Vector2 toPosition)
        {
            return (toPosition - fromPosition).normalized;
        }
    }
}